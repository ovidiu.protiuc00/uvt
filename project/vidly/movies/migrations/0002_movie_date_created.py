# Generated by Django 2.2.16 on 2021-03-03 15:59

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('movies', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='movie',
            name='date_created',
            field=models.DateTimeField(default=datetime.datetime(2021, 3, 3, 15, 59, 5, 861059, tzinfo=utc)),
        ),
    ]
