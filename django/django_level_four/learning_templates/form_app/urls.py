from django.urls import path
from django.urls.conf import include
from form_app import views

app_name = 'form_app'

urlpatterns = [
    path('a/', views.form, name="index"),
]