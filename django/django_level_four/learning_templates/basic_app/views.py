from django.shortcuts import render
import datetime


def index(request):
    return render(request, "basic_app/index.html")

def other(request):
    context_dict = {'name':'John"', 'age':100, 'date': datetime.datetime.now()}
    return render(request, "basic_app/other.html", context_dict)

def relative(request):
    return render(request, "basic_app/relative_url_templates.html")


# Create your views here.
