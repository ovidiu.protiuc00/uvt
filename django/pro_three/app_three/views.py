from django.shortcuts import render
from . import forms

def index(request):
    return render(request, 'pro_three/index.html')

def forms_by(request):
    form = forms.FormName
    return render(request, 'pro_three/forms.html', {'forms' : form})