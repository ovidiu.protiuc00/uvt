from django import forms
from django.forms.widgets import Textarea

class FormName(forms.Form):
    name = forms.CharField(max_length=256, required = True)
    email = forms.EmailField(required=False, unique = True)
    test = forms.CharField(required=True, widget=Textarea)