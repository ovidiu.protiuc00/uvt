"""my_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from my_app import views
app_name = 'my_app'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('parents/', views.ParentList.as_view(), name='parent_list'),
    path('parent_create/', views.ParentCreate.as_view(), name='parent_create'),
    path('parents/<str:pk>/', views.ParentDetail.as_view(), name='parent_detail'),
    path('delete/<str:pk>', views.ParentDelete.as_view(), name="parent_delete"),
    path('register/', views.register_request, name="register"),
    path('login/', views.login_request, name="login"),
    path('logout/', views.logout_request, name="logout"),
    path('my_req/', views.my_req, name='my_req'),
    path('error_req/', views.error_req, name='error_req'),
]
