from django.views.generic import (
    TemplateView, DetailView, ListView, CreateView, DeleteView
)
from django.shortcuts import get_object_or_404
from my_app.models import Parent,Child
from my_app.forms import  NewUserForm, ParentForm, ChildForm
from django.views.generic.edit import FormView
from django.urls import reverse_lazy
from django.contrib.auth import login,authenticate,logout
from django.contrib import messages
from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm #add this
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.mail import send_mail

import logging

logger = logging.getLogger('my_app.views')

class IndexView(TemplateView):
    template_name = 'index.html'    

class ParentList(ListView):
    template_name = 'parent_list.html'
    context_object_name = 'parent_list'
    def get_queryset(self):
        return Parent.objects.all()


class ParentDetail(DetailView):
    template_name = 'parent_detail.html'
    context_object_name = 'parent_detail'
    model = Parent



class ParentCreate(LoginRequiredMixin, CreateView):
    login_url = 'my_app:login'
    redirect_field_name = 'parent_list'
    template_name = 'parent_create.html'
    model = Parent
    fields = ['first_name', 'last_name', 'nationality']

class ParentDelete(DeleteView):
    template_name = 'delete.html'
    model = Parent
    success_url = reverse_lazy('my_app:parent_list')

def register_request(request):
    print(1)
    if request.method == 'POST':
        print(2)
        form = NewUserForm(request.POST)
        if form.is_valid():
            print(3)
            user = form.save(True)
            login(request, user)
            messages.success(request, "Registration Successful.")
            return redirect("my_app:index")
        else:
            print(4)
            messages.error(request, "Unsuccessfull registering. Invalid information!")
    print(5)
    form = NewUserForm()
    return render(request=request, template_name="register.html", context={"register_form": form})

def login_request(request):
    logger.error(0)
    if request.method == 'POST':
        logger.error(1)
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            logger.error(2)
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username = username,password = password)
            if user != None:
                logger.error(3)
                login(request, user)
                messages.info(request, f'Successfully logged in {username}')
                return redirect('my_app:index')
            else:
               logger.error(4)
               messages.error(request, "Unsuccessfull logging. Invalid information!")
        else:
            logger.error(1)
            messages.error(request, "Invalid username or password")
    logger.error(6)
    form = AuthenticationForm()
    return render(request=request, template_name="login.html", context={'form':form})

def logout_request(request):
    logout(request)
    messages.add_message(request, messages.INFO, '{0} logged out.'.format(request.user))
    messages.info(request, "Succesfully logged out.")
    return redirect('my_app:index')


def my_req(request):
    logger.info('Succesfull ')
    form = Parent.objects.raw('SELECT * FROM qvendor1_1.Exchanges limit 10000;')
    messages.success(request, 'Select ran successful1')
    messages.info(request, 'Select ran successful2')
    messages.error(request, 'Select ran successful3')
    messages.error(request, 'Select did not run successful4')
    # send_mail('This is the first mail', 'It s very important for you to see this', 'protiuc20@gmail.com', 
    #   ['ovidiu.protiuc00@e-uvt.ro', 'protiuc.ovidiu@yahoo.com'], fail_silently=False)
    return render(request=request, template_name="my_req.html", context={'form':form})

def error_req(request):
    print('aici')
    logger.error('Something went wrong!')
    print(__name__)
    return render(request=request, template_name="index.html")

# Create your views here.
