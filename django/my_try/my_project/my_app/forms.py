from django.forms import ModelForm
from django import forms
from my_app.models import Parent,Child
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class ParentForm(ModelForm):
    class Meta:
        model = Parent
        fields = ['first_name', 'last_name', 'nationality']

class ChildForm(ModelForm):
    class Meta:
        model = Child
        fields = ['first_name', 'last_name', 'email']


class NewUserForm(UserCreationForm):
    email = forms.EmailField(required=True)
    class Meta():
        model = User
        fields = ['username', 'email']
    def save(self, commit = False):
        user = super(NewUserForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        if commit:
            user.save()
        return user
