from django.db import models
from django.db.models.fields import CharField, EmailField
from django.db.models.fields.related import ForeignKey
from django.urls import reverse

class Parent(models.Model):
    first_name = CharField(max_length=100)
    last_name = CharField(max_length=100)
    nationality = CharField(max_length=100)
    def __str__(self) -> str:
        return self.first_name +' '+ self.last_name
    def get_absolute_url(self):
        return reverse("my_app:parent_detail", kwargs={"pk": self.pk})
    

class Child(models.Model):
    first_name = CharField(max_length=100)
    last_name = ForeignKey(Parent, on_delete=models.CASCADE)
    email = EmailField(max_length=500)
    def __str__(self) -> str:
        return self.first_name +' '+ self.last_name
