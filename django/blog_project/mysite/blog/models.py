from django.db import models
from django.utils import timezone
from django.urls import reverse

class Post(models.Model):
    author = models.ForeignKey('auth.User', on_delete=True)
    title = models.CharField(max_length=100)
    text = models.TextField(max_length=10000)
    create_date = models.DateTimeField(timezone.now())
    published_date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.author

    def publish(self):
        self.published_date = timezone.now()
        self.save()
    
    def approve_comment(self):
        return self.comments.filter(approved_comment=True)
    
    def get_absolute_url(self):
        return reverse("post_detail", kwargs={"pk": self.pk})
    


class Comment(models.Model):
    post = models.ForeignKey('blog.Post', on_delete=True)
    author = models.CharField(max_length=100)
    text = models.TextField(max_length=1000)
    create_date = models.DateTimeField(timezone.now())
    approved_comment = models.BooleanField(default=False)

    def approve(self):
        self.approved_comment = True
        self.save()
    
    def __str__(self):
        return self.text
    
    def get_absolute_url(self):
        return reverse("post_list")
    
