from pools.models import Question, Choice
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views.generic import ListView,DetailView

from django.template import loader
from django.http import Http404
from django.shortcuts import get_object_or_404, render


class IndexView(ListView):
    template_name = 'pools/index.html'
    context_object_name = 'latest_question_list'
    def get_queryset(self):
        return Question.objects.order_by('pub_date')[:5]

class DetailsView(DetailView):
    model = Question
    template_name = 'pools/detail.html'

class ResultsView(DetailView):
    model = Question
    template_name = 'pools/results.html'

# def index(request):
#     latest_questions = Question.objects.order_by('pub_date')[:5]
#     print(latest_questions)
#     context = {'latest_questions': latest_questions}
#     return render(request, 'pools/index.html', context)

# def detail(request, question_id):
#     try:
#         question = Question.objects.get(pk=question_id)
#     except Question.DoesNotExist:
#         raise Http404("Question does not exist")
#     return render(request, 'pools/detail.html', {'question': question})

# def results(request, question_id):
#     try:
#         question = Question.objects.get(pk = question_id)

#     except Question.DoesNotExist:
#         raise Http404("Not exist")
#     dict = {
#         'question' : question,
#         'date' : question.pub_date.strftime("%Y-%m-%d %H:%M:%S"),
#     }
#     return render(request, 'pools/detail.html', dict)

def other(request, question_id):
    q = get_object_or_404(Question, pk = question_id)
    dict = {
    'question' : q.question_text,
    'date' : q.pub_date.strftime("%Y-%m-%d %H:%M:%S"),
    }
    return render(request, 'pools/other.html', dict)

def vote_results(request, question_id):
    q = get_object_or_404(Question, pk = question_id)
    dict = {
    'question' : question_id,
    'date' : q.pub_date.strftime("%Y-%m-%d %H:%M:%S"),
    }
    return render(request, 'pools/results.html',  {'question': q})

def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        print(1)
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'pools/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })

    print(2)
    selected_choice.votes += 1
    selected_choice.save()
    # Always return an HttpResponseRedirect after successfully dealing
    # with POST data. This prevents data from being posted twice if a
    # user hits the Back button.
    return HttpResponseRedirect(reverse('pools:results', args=(question.id,)))

# Create your views here.
