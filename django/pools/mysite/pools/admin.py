from django.contrib import admin
from pools.models import Question,Choice

admin.site.register(Question)
admin.site.register(Choice)
# Register your models here.
