from django.shortcuts import render
from django.http import HttpResponse
from app_two.models import User

# def index(request):
#     return HttpResponse("<em>Hello buna</em>")

# def help(request):
#     var = {'var' : 'This is my var'}
#     return render(request, 'app_two/index.html', context=var)

def users(request):
    users = User.objects.order_by('first_name')
    user_dict = {'users' : users}
    return render(request, 'app_two/index.html', context=user_dict)

# Create your views here.
