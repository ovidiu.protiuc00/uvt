from typing import Text
from django import forms
from django.core.exceptions import ValidationError
from django.forms.widgets import Textarea

class FormName(forms.Form):
    name = forms.CharField(max_length=256, required = True)
    email = forms.EmailField(required=False)
    test = forms.CharField(required=True, widget=Textarea)
    botcatcher = forms.CharField(required=False, widget=forms.HiddenInput)

    def clean_botcatcher(self):
        botcatcher = self.cleaned_data['botcatcher']
        if len(botcatcher) > 0:
            raise ValidationError("Gotcha BOT")
        return botcatcher