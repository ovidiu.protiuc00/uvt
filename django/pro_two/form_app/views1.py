from django.shortcuts import render
from . import forms

def index(request):
    return render(request, 'app_two/index.html')

def forms_by(request):
    form = forms.FormName
    if request.method == 'POST':
        form = forms.FormName(request.POST)
        if form.is_valid():
            print('Valid data: \n')
            print(form.cleaned_data['name'])

    return render(request, 'app_two/forms.html', {'forms' : form})