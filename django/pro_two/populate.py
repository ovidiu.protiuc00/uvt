import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE','pro_two.settings')

import django
django.setup()

import random
from app_two.models import User
from faker import Faker

fake_gen = Faker()


def populate(N=5):
    for i in range(N):
        fake_first_name = fake_gen.name()
        fake_last_name = fake_gen.name()
        fake_email = fake_gen.email()

        user = User.objects.get_or_create(first_name = fake_first_name, last_name = fake_last_name, 
            email = fake_email)
        

if __name__ == '__main__':
    print('Populating script!')
    populate(20)
    print("Complete")