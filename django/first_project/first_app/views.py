from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    my_var = {'insert_me':'New var in view'}
    return render(request,'first_app/index.html', context=my_var)

def index1(request):
    return HttpResponse("Hellor World1")

# Create your views here.
