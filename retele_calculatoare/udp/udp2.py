import socket
import time

client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP) # UDP
client.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)

# Enable broadcasting mode
client.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

client.bind(("", 37020))

while True:
    data, addr = client.recvfrom(1024)
    message = 'Da'
    time.sleep(1)    
    client.sendto(message.encode('utf-8'), ('<broadcast>', 37020))
    print("received message: %s"%data.decode('utf-8'))