import socket
import struct

HOST = '127.0.0.1' # equivalent to 0.0.0.0
PORT = 12337
#MSG_DELIM = b'\0'
#RCV_BYTES = 4096

header_len = struct.Struct('!I')  # unsigned int, network byte order (big endian)

def create_listen_socket(host, port):
    """ Setup the sockets our server will receive connection
    requests on """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind((host, port))
    # no 2 sockets can bind on the same (host, port) at the same time
    sock.listen(100)
    return sock

def recv_all(sock, length):
    """ Receive length bytes from socket; blocking """
    data = bytearray()

    while length:
        recvd = sock.recv(length)
        if not recvd:
            # Socket has been closed prematurely
            raise ConnectionError()

        data = data + recvd
        length = length - len(recvd)

    return data

def recv_msg(sock):
    """ Wait for data to arrive on the socket, then parse 
    messages into length + actual data """

    lungime = recv_all(sock, header_len.size)
    (lungime,) = header_len.unpack(lungime)

    # Read lung bytes
    msg = recv_all(sock, lungime)
    msg = msg.decode('utf-8') 
    return msg

def prep_msg(msg1):
    """ Prepare a string to be sent as a message """
    msg = str(msg1)
    lungime = len(msg)
    actual_msg = header_len.pack(lungime)
    actual_msg += msg.encode('utf-8')
    print('{}'.format(actual_msg))
    return actual_msg

def send_msg(sock, msg):
    """ Send a string over a socket, preparing it first """
    data = prep_msg(msg)
    sock.sendall(data)
    # sock.send(data) -> not guaranteed to send all data