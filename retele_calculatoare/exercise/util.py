import socket
import struct

HOST = '' # equivalent to 0.0.0.0
PORT = 4040
#MSG_DELIM = b'\0'
#RCV_BYTES = 4096

header_len = struct.Struct('!I')  # unsigned int, network byte order (big endian)

def create_listen_socket(host, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind((host, port))
    # no 2 sockets can bind on the same (host, port) at the same time
    sock.listen(100)
    return sock

def recv_all(sock, length):
    data = bytearray()
    recvd = sock.recv(1024)
    data = data + recvd
    return data

def recv_msg(sock):
    msg = recv_all(sock, 0)
    msg = msg.decode('utf-8') 
    return msg

def prep_msg(msg):
    return  msg.encode('utf-8')

def send_msg(sock, msg):
    data = prep_msg(msg)
    sock.sendall(data)
