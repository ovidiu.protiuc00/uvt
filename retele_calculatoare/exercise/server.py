import util,time
import threading


HOST = util.HOST
PORT = util.PORT

def handle_client(sock, addr):
    try:
        msg = util.recv_msg(sock) # Blocks until received
        # complete message 
        print('{}: {}'.format(addr, msg))
        util.send_msg(sock,msg)
    except (ConnectionError, BrokenPipeError):
        print('Closed connection to {}'.format(addr))
        sock.close()
    return msg


if __name__ == '__main__':
    listen_sock = util.create_listen_socket(HOST, PORT)
    addr = listen_sock.getsockname()
    print('Listening on {}'.format(addr))
    while True:
        client_sock, addr = listen_sock.accept()
        msg = handle_client(client_sock,addr)
        print('{}: {}'.format(addr, msg))
        client_sock, addr = listen_sock.accept()
        x = chr(ord(msg) + 3) 
        util.send_msg(client_sock,x)
    

    