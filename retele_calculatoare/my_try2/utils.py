import socket

HOST = '127.0.0.1'
PORT = 12338


def create_listen_socket(HOST, PORT):
    sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
    sock.bind((HOST,PORT))
    sock.listen(200)
    return sock

def recv_msg(sock):
    msg = sock.recv(10)
    msg = msg.decode('utf-8')
    return msg

def send_msg(sock, msg):
    sock.sendall(msg.encode('utf-8'))
