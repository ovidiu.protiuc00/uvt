import utils, sys, socket

HOST = utils.HOST
PORT = utils.PORT

if __name__ == '__main__':
    try:
        sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        sock.connect((HOST,PORT))
    except ConnectionError:
        print('Connection Error')
        sys.exit(1)
    
    print('\nConnected to {}:{}'.format(HOST, PORT))
    print("Type message, enter to send, 'q' to quit")


    while 1:
        msg = input()
        if msg == 'q': break
        try:
            utils.send_msg(sock,msg)
            print('CLIENT send {} '.format(msg))
            utils.recv_msg(sock)
            print('CLIENT receive {} '.format(msg))
        except ConnectionError:
            print('Socket error during communication')
            sock.close()
            print('Closed connection to server\n')
            break
    
    sock.close()