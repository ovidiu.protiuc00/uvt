import utils

HOST = utils.HOST
PORT = utils.PORT

def handle_client(client_sock, addr):
    while 1:
        try:
            message = utils.recv_msg(client_sock)
            print('SERVER receive {} '.format(message))
            utils.send_msg(sock,'altceva')
            print('SERVER send {} '.format('altceva'))
        except(ConnectionError, BrokenPipeError):
            print('Closed connection to {}'.format(addr))
            client_sock.close()
            break

if __name__ == '__main__':
    sock = utils.create_listen_socket(HOST, PORT)
    while 1:
        client_sock,addr = sock.accept()
        handle_client(client_sock, addr)