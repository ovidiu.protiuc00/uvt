import socket
import struct

HOST = '' # equivalent to 0.0.0.0
PORT = 4040
#MSG_DELIM = b'\0'
#RCV_BYTES = 4096

header_len = struct.Struct('!I')  # unsigned int, network byte order (big endian)

def create_listen_socket(host, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind((host, port))
    # no 2 sockets can bind on the same (host, port) at the same time
    sock.listen(100)
    return sock

def recv_all(sock, length):
    data = bytearray()

    while 1:
        recvd = sock.recv(1024)
        print("{}".format(recvd))
        for i in recvd:
            if i==b'\0':
                data = data + recvd[i]
                break
        break
        # length = length - len(recvd)
    
    return data



def recv_msg(sock):
    # lungime = recv_all(sock, header_len.size)
    # (lungime,) = header_len.unpack(lungime)
    # print("lungime{}".format(lungime))
    msg = recv_all(sock, 0)
    msg = msg.decode('utf-8') 
    return msg

def prep_msg(msg):
    # lungime = len(msg)
    # actual_msg = header_len.pack(lungime)
    # actual_msg += msg.encode('utf-8')
    # return actual_msg
    return  msg.encode('utf-8')

def send_msg(sock, msg):
    data = prep_msg(msg)
    sock.sendall(data)

