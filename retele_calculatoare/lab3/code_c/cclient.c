#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

#define MAX_LINE_SIZE 255
#define MESSAGE "Hello, world\n"

int main(int argc, char **argv) 
{
  int cfd = 0;
  int port_number = 0;
  char data[MAX_LINE_SIZE]; 

  struct sockaddr_in server_sock_addr;

  
  if (argc != 3) {
    fprintf(stderr, "Invalid syntax: %s <addr> <port>\n", argv[0]);
    exit(1);
  }

  port_number = strtol(argv[2], NULL, 10);

  if (port_number == 0) {
    fprintf(stderr, "Invalid port number !\n");
    exit(1);
  }

  if (!strncmp(argv[1], "0.0.0.0", strlen("0.0.0.0")))
  {
    fprintf(stderr, "Invalid server address %s\n", argv[1]);
    exit(1);
  }



  memset(&server_sock_addr, 0, sizeof(server_sock_addr));
  server_sock_addr.sin_family = AF_INET;
  if (!inet_pton(AF_INET, argv[1], &server_sock_addr.sin_addr))
  {
	  fprintf(stderr, "Invalid network address %s\n", argv[1]);
	  exit(1);
  }
  server_sock_addr.sin_port = htons(port_number);



  if (cfd == -1)
  {
    fprintf(stderr, "Failed to create socket");
    exit(1);
  }


  cfd = socket(AF_INET, SOCK_STREAM, 0);
  if (cfd == -1)
  {
	  fprintf(stderr, "Error creating socket\n");
	  exit(1);
  }

  if (connect(cfd, (struct sockaddr*) &server_sock_addr, sizeof(server_sock_addr)) == -1)
  {
    fprintf(stderr, "Failed to connect to %s on port %s\n", argv[1], argv[2]);
    exit(1);
  }

  if (write(cfd, MESSAGE, strlen(MESSAGE)) != strlen(MESSAGE))
  {
	  fprintf(stderr, "Error writing message!\n");
	  exit(2);
  }

  int leftToRead = MAX_LINE_SIZE;
  int actRead = 0;

  while ((actRead = read(cfd, data + MAX_LINE_SIZE - leftToRead, leftToRead - 1)) > 0 )
  {
    leftToRead -= actRead;
    

    if (actRead == -1)
    {
      fprintf(stderr, "Error reading from socket\n");
      exit(3);
    }

    data[MAX_LINE_SIZE - leftToRead ] = '\0';

    printf("Received: %s\n", data);

    if (strchr(data, '\n'))
  	  break;
  }


  if (write(cfd, "exit\n", strlen("exit\n")) != strlen("exit\n"))
  {
	  fprintf(stderr, "Error sending exit command!\n");
	  exit(2);
  }

  leftToRead = MAX_LINE_SIZE ;
  while ((actRead = read(cfd, data + MAX_LINE_SIZE - leftToRead, leftToRead - 1)) > 0 )
  {
    leftToRead -= actRead;

    if (actRead == -1)
    {
      fprintf(stderr, "Error reading from socket\n");
      exit(3);
    }

    data[MAX_LINE_SIZE - leftToRead] = '\0';

    printf("Received: %s\n", data);
    if (strchr(data, '\n'))
  	  break;
  }
  close(cfd);

  return 0;

}
