#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

#define MAX_LINE_SIZE 255

int sockReadLine(int sfd, void* buffer, size_t n)
{
    int numRead, totalRead;
    char* buf;
    char c;

    if (n <= 0 || buffer == NULL)
    {
        fprintf(stderr, "Invalid arguments!\n");
        return -1;
    }

    buf = buffer;
    totalRead = 0;

    for (;;)
    {
        numRead = read(sfd, &c, 1);

        if (numRead == -1)
        {
            fprintf(stderr, "Read error on socket\n");
            return -1;
        }
        else if (numRead == 0)
        {
            if (totalRead == 0)
                return 0;
            else
                break;
        }
        else
        {
            if (totalRead < n -1)
            {
                totalRead++;
                *buf++ = c;
            }

            if (c == '\n')
                break;
        }
    }

    *buf = '\0';

    return totalRead;
}

int main(int argc, char **argv) {
  int server_socket = 0;
  int port_number = 0;
  int ret;
  char goodbye[MAX_LINE_SIZE];
  memset(goodbye, 0, MAX_LINE_SIZE);
  
  struct sockaddr_in server_sock_addr;

  
  if (argc != 2) {
    fprintf(stderr, "Invalid syntax: %s <port>\n", argv[0]);
    exit(1);
  }

  port_number = strtol(argv[1], NULL, 10);
  if (port_number == 0) {
    fprintf(stderr, "Invalid port number !\n");
    exit(1);
  }

  server_socket = socket(AF_INET, SOCK_STREAM, 0);
  if (server_socket == -1)
  {
    fprintf(stderr, "Error creating socket on port %d\n", port_number);
    exit(2);
  }

  memset(&server_sock_addr, 0, sizeof(struct sockaddr_in));
  server_sock_addr.sin_family = AF_INET;
  server_sock_addr.sin_port = htons(port_number);
  inet_pton(AF_INET, "0.0.0.0", &server_sock_addr.sin_addr);


  int sock_option = 1;
  fprintf(stderr, "Binding socket to 0.0.0.0 port %d\n", port_number);
  setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &sock_option, sizeof(sock_option));
  setsockopt(server_socket, SOL_SOCKET, SO_REUSEPORT, &sock_option, sizeof(sock_option));
  ret = bind(server_socket, (const struct sockaddr *) &server_sock_addr, sizeof(server_sock_addr));

  if (ret == -1) {
    perror("Failed to bind");
    exit(1);
  }



  fprintf(stderr, "Starting to listen!\n");
  ret = listen(server_socket, 10);
  if (ret == -1) {
    perror("Failed to listen");
    exit(1);
  }

  fprintf(stderr, "Waiting for clients...\n");
  while (1) 
  {
    int client_sock = 0;
    struct sockaddr_in client_sock_addr;
    socklen_t client_sock_addr_len = sizeof(client_sock_addr);
    char *client_addr_str;
    char line[MAX_LINE_SIZE];
    memset(line, 0, MAX_LINE_SIZE);
    int client_port;
    
    client_sock = accept(server_socket, (struct sockaddr *) &client_sock_addr, &client_sock_addr_len);
    client_addr_str = inet_ntoa(client_sock_addr.sin_addr);
    client_port = ntohs(client_sock_addr.sin_port);
    fprintf(stderr, "New client connected from: %s port %d\n", client_addr_str, client_port);


    while ((ret = sockReadLine(client_sock, line, MAX_LINE_SIZE)) > 0) 
    {
        if (ret == -1)
        {
            fprintf(stderr, "Could not read line!\n");
            goto end;
        }

        printf("Received: %s\n", line);

        if (strncmp(line, "exit", 4) == 0) 
        {
            snprintf(goodbye, MAX_LINE_SIZE, "Bye Bye %s:%d\n", client_addr_str, client_port);

            if (write(client_sock, goodbye, strlen(goodbye)) != strlen(goodbye))
                fprintf(stderr, "Error saying goodbye\n");

            goto end;
        }

        if (write(client_sock, line, strlen(line)) != strlen(line))
        {
            fprintf(stderr, "Error writing to client socket\n");
            goto end;
        }

        if (ret == 0)
            break;

    }

  end:
    fprintf(stderr, "Saying gododbye to client  %s:%d\n", client_addr_str, client_port);
      close(client_sock);
  }  
}
