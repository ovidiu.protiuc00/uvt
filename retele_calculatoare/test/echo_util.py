import socket
import struct

HOST = '' # equivalent to 0.0.0.0
PORT = 1337

header_len = struct.Struct('!I') 

def create_listen_socket(host, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind((host, port))
    # no 2 sockets can bind on the same (host, port) at the same time
    sock.listen(15)
    return sock

def recv_all(sock, length):
    """ Receive length bytes from socket; blocking """
    data = bytearray()

    while length:
        recvd = sock.recv(length)
        if not recvd:
            # Socket has been closed prematurely
            raise ConnectionError()

        data = data + recvd
        length = length - len(recvd)

    return data

def recv_msg(sock):
    lungime = recv_all(sock, header_len.size)
    (lungime,) = header_len.unpack(lungime)

    # Read lung bytes
    msg = recv_all(sock, lungime)
    msg = msg.decode('utf-8') 
    return msg

def prep_msg(msg):
    lungime = len(msg)
    actual_msg = header_len.pack(lungime)
    actual_msg += msg.encode('utf-8')
    return actual_msg

def send_msg(sock, msg):
    data = prep_msg(msg)
    sock.sendall(data)