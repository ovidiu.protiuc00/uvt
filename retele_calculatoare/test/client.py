import sys, socket
import echo_util
from datetime import date


HOST = sys.argv[-1] if len(sys.argv) > 1 else '127.0.0.1'
PORT = echo_util.PORT

if __name__ == '__main__':
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((HOST, PORT))  #connect to server
    except ConnectionError:
        print('Socket error on connection')
        sys.exit(1)

    print('\nConnected to {}:{}'.format(HOST, PORT))
    try:
        msg = echo_util.recv_msg(sock)
        print('Received Date')
        today = date.today()
        d1 = today.strftime("%Y/%m/%d")
        msg_to_send = d1
        if msg == d1:
            msg_to_send = 'OK'
            print('Dates match')
        else:
            print('Dates do not match')
        echo_util.send_msg(sock, msg_to_send)
    except ConnectionError:
        print('Socket error during communication')
        sock.close()
        print('Closed connection to server\n')
            
    print("Closing connection")
    sock.close()