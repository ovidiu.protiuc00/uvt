import echo_util
import threading
from datetime import date

HOST = echo_util.HOST
PORT = echo_util.PORT

def handle_client(sock, addr):
    try:
        print('Accept client {}'.format(addr))
        today = date.today()
        d1 = today.strftime("%Y/%m/%d")
        echo_util.send_msg(sock, d1) 
        print('Sent date')
        msg = echo_util.recv_msg(sock) 
        print('Received {}'.format(msg))
        print('Closed connection')
    except (ConnectionError, BrokenPipeError):
        print('Closed connection to {}'.format(addr))
        sock.close()
        

if __name__ == '__main__':
    listen_sock = echo_util.create_listen_socket(HOST, PORT)
    addr = listen_sock.getsockname()
    print('Listening on {}'.format(addr))
    while True:
        client_sock, addr = listen_sock.accept()
        # Thread will run function handle_client() autonomously
        # and concurrently to this while loop
        thread = threading.Thread(target = handle_client, args = [client_sock, addr], daemon=True)
        thread.start()