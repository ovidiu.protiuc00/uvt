#!/usr/env/bin python
import socket
TCP_IP = '127.0.0.1'
TCP_PORT = 12338
BUFFER_SIZE = 50

my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
my_socket.bind((TCP_IP,TCP_PORT))
my_socket.listen(100)

while 1:
    conn, addr = my_socket.accept()
    print('Connection address', addr)
    data = conn.recv(BUFFER_SIZE)
    conn.send(data)
conn.close()