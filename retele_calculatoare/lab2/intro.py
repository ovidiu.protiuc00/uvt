# -*- coding: utf-8 -*-

import socket
import sys

def main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect( ("neverssl.com", 80) )
    
    fd = sock.makefile()
    
    fd.write("GET / HTTP/1.1\n")
    fd.write("Host: neverssl.com\n")
    fd.write("User-Agent: botReteleCalculatoare/1.1\n")
    fd.write("Accept: */*\n")
    fd.write("\n")
    fd.flush()
    
    line = fd.readline()
    if line is None:
        print "Fara raspuns"
        sys.exit(1)
    line = line.strip()
    http_version, status_code, status_message = line.split(" ", 2)
    print "Versiune http:", http_version
    print "Stare http:", status_code
    print "Mesaj stare:", status_message
    
    headers = {}
    # În while-ul acesta citim doare headere
    while True:
        linie = fd.readline()
        if linie is None:
            break
        linie = linie.strip()
        if linie == "":
            break
        print(linie)
        
        header_name, header_value = linie.split(":", 1)
        headers[header_name] = header_value
     
    content_length = int(headers["Content-Length"])
    data = fd.read(content_length) 
    
    with open("my.html", "w") as f:
        f.write(data)
    
    sock.close()

if __name__ == "__main__":
    main()
