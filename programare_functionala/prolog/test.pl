%1
sum([ ] , 0).
sum([X|T] ,W) :-
    sum(T, S), W is X+S.

%see('./problema1.txt'), read(A), read(B), sum([A,B], S), write(S), seen.

%2
fact(0,1).
fact(N,F):-
  N1 is N-1,
  fact(N1,F1),
  F is N*F1.

% ?-fact(3,F).
