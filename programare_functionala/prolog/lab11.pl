%see('./fis.txt'), read(X), read(Y), read(Z), sum([X,Y,Z], S), write(S), seen.

sum([ ] , 0).
sum([X|T] ,W) :-
    sum(T, S), W is X+S.

% write pe fisier
% tell('sum.txt'), write('dap'), nl, told.

%b
%see('./fis.txt'), read(X), read(Y), read(Z), sum([X,Y,Z], S), tell('sum.txt'), write(S), nl, write(''), told, seen.




%5
my_randa(X, Y, L) :-
 length(L, X), maplist(random(0,Y), L).

%2.5
%see('./pb.txt'), read(X), read(Y), read(Z), sum([X,Y,Z], S), tell('sum.txt'), write(S), nl, write(''), told, seen.

even(X):-
    X mod 2 =:= 0,
    write(X).

odd(X):-
    X mod 2 =:= 1,
    write(X).

%see('./pb.txt'), read(X), read(Y), read(Z), tell('even.txt'), even(X), even(Y), even(Z), nl, told ,seen.



%lab11 
%cut is a goal (A goal is something that Prolog tries to satisfy by finding values of the variables)
teaches(dr_fred, history).
teaches(dr_fred, english).
teaches(dr_fred, drama).
teaches(dr_fiona, physics).	         	
studies(alice, english).
studies(angus, english).
studies(amelia, drama).
studies(alex, physics).

%teaches(dr_fred, Course), studies(Student, Course). cut nu e folosit 
%teaches(dr_fred, Course), !, studies(Student, Course). cut e folosit, se incheie totul la history, nimeni nu studiaza istoria => returneaza false
%teaches(dr_fred, Course), studies(Student, Course), !. cut e folosit, se incheie la primul false, english alice 
%!, teaches(dr_fred, Course), studies(Student, Course).

%cut in reguli 

max1(X, Y, X) :- X > Y, !.
max1(_X, Y, Y). %X_ e folosit ca sa evitam singleton pattern 

max2(X, Y, X) :- X > Y.
max2(X, Y, Y) :- X =< Y. 

%max1(6, 3, Max).
%max1(3, 6, Max).
%max1(6, 3, 3).