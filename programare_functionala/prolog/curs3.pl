sum_to(N,1):-N=<1,!.
sum_to(N,Res):-N1 is N-1,
    sum_to(N1,Res1),
    Res is Res1+N.


no(P):-P,!,fail.
no(_).

cappend([],X,X):-!.
cappend([H|T],C,[H|T2]):-
    cappend(T,C,T2).

% orice student care participa la cursul de PLF nu participa la
% cursul de literatura
% Toti ceilalti studenti participa la cursul de literatura.
%
%
student(alin).
student(andreea).
student(mihai).
student(mihaela).

participa_PLF(mihaela).
participa_PLF(mihai).

participa_literatura(X):-student(X),participa_PLF(X),!,fail.
participa_literatura(X):-student(X).