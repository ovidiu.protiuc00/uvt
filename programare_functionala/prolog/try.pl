mama(ion, maria).
mananca(ion, carne).
mananca(maria, piftea).
mananca(ion, ceva).
mananca(ion, ceva1).


hot(gigel).
place(maria, ciocolata).
place(maria, vin).
place(gigel, X) :-
                place(X, vin).
                place(x, ciocolata).

poate_fura(X, Y) :-
                hot(X),place(X,Y).

loves(juliet, romeo).

loves(romeo , juliet) :- loves(juliet, romeo).


male(ion).
male(marius).

female(alice).
female(betsy).

happy(albert).
happy(alice).
happy(bob).
happy(betsy).
with_albert(albert).
%with_albert(alice).

runs(albert) :- happy(albert).
dance(bob).

dance(alice) :-
    happy(alice),
    with_albert(alice).

does_alice_dance :- dance(alice),
    write('When Alice is happy and with albert she dance').







parent(albert, bob).
parent(albert, betsy).
parent(albert, bill).


parent(alice, bob).
parent(alice, betsy).
parent(alice, bill).





parent(bob, carl). 
parent(bob, charlie).
parent(betsy, maciu).


%recursion

related(X, Y) :-
    parent(X, Y).

related(X, Y) :-
    parent(X, Z),
    parent(Z, Y).


get_grand_c :-
    parent(albert, X),
    parent(X, Y),
    write('Copii sunt:'),
    write(Y), nl.





grade(5) :-
    write('Go to kindergarden').



grade(6) :-
    write('Go to 1st class').

grade(Other) :-
    Grade is Other,
    format('Go to grade ~w', [Grade]).





has(albert, olive).

owns(albert, pet(cat, olive)).



client(sally, smith, 213.42).
client(sally, smith, 213.4232).

vertical(line(X, Y), point(Y, Y2)).


warm_blood(human).
warm_blood(penguin).

has_milk(human).
has_milk(penguin).

has_hair(human).
has_feathers(penguin).

mammal(X) :-
    warm_blood(X),
    has_milk(X),
    has_hair(X).

p([1, 2, 4]).

pa([2,3, 4], [32]).


double_digit(X, Y) :-
    Y is X*2.



count_to(X):-
    X =< 10,
    write(X), nl,
    Y is X+1,
    count_to(Y).

sum([ ] , 0).
sum([X|T] ,W) :-
    sum(T, S),
    W is X+S.

greater(X, L):-
    maplist(>(X), L).

add_elem_in_list(L, E, R) :- append(L, [E], R).





max(X,Y,Max):- X>=Y, !, Max=X;
    Max=Y.



membru(X, [X|_]).

membru(X,[_|Y]):-
    membru(X, Y).




day(1,monday).
day(2,tuesday).
day(3,wednesday).
day(4,thursday).
day(5,friday).

next_day(5,monday):- !.

next_day(N,X):-
    Next is N+1,
    day(Next,X).


grade(popescu,'LCS',9.0).
grade(cin,'MMS',9.0).
grade(inu,'LVR',4.0).
grade(guta,'VVE',6.0).

sum([], 0).

sum([H|T],S):-
    sum(T,S1),
    S1 is S+H.




member(X,[X,_]).
member(X,[_|B]):-
    member(X,B), !,fail.


 
remove_vowels(INPUTz0,OUTPUTz0)
:-
prolog:phrase(remove_vowels(INPUTz0),OUTPUTz0)
.
 
remove_vowels([])
-->
[]
.
 
remove_vowels([INPUT0|INPUTz0])
-->
{ vowel(INPUT0) } ,
! ,
remove_vowels(INPUTz0)
.
 
remove_vowels([INPUT0|INPUTz0])
-->
! ,
[INPUT0] ,
remove_vowels(INPUTz0)
.
 
vowel(CHAR):-
    lists:member(CHAR,"AEIOUaeiou").



s([],X,X) :- !.

s(X,[],X) :- !.

s([X|Y],[Z|W],[X|V]) :- X=<Z,!,s(Y,[Z|W],V).

s([X|Y],[Z|W],[Z|V]) :- Z<X,s([X|Y],W,V).



:-op(200,yfx,+).

:-op(400,yfx,*).

f(X,Y):-Y is X**2+5*6-1.



p(C,[C|_]).

p(C,[_|B]) :- p(C,B).



b2(A,B,C):-b(A,[],[],B,C).

b([],X,Y,X,Y).

b([A],X,Y,A1,A2):-a(A,X,N), b([],N,Y,A1,A2),!.

b([A,B|X],X,Y,A1,A2):-a(A,X,N1), a(B,Y,N2), b(X,N1,N2,A1,A2).



a(I,[],[I]):-!.

a(I,[X|Y],[X|Z]):-a(I,Y,Z).