
l = integer*

printlist(l)

printlist([]).

printlist([X|List]) :-
    write(X),nl,
    printlist(List), !,fail.




mama(ana).
mama(mia).

copil(mia,meli).
copil(mia,darius).
copil(ana,andreea).

este_mama(X):-
    mama(X).

este_copil(X,Y):-
    copil(X,Y).


sunt_frati(Y,Z):-
    copil(X,Y),
    copil(X,Z).



count(Low,High):-
    between(Low,High,Y),
    Z is Low+Y,
    write(Z),nl.


guess_number():- loop(start).

loop(15) :- write('This is the number').


loop(X):-
    X \= 15,
    write('Number: '),
    read(Guess),
    write(Guess),
    write(' is not the number'),nl,
    loop(Guess).


is_even(X) :- Y is X//2, X =:= 2 * Y.




say_hi:-
    write('Hi'),nl,
    read(X),
    write(X).


fav_char :-
    write('Your fav characher is : '),nl,
    get(X),
    format('The ascii value is ~w', [X]),nl,
    put(X).




%lab11 
%cut is a goal (A goal is something that Prolog tries to satisfy by finding values of the variables)
teaches(dr_fred, history).
teaches(dr_fred, english).
teaches(dr_fred, drama).
teaches(dr_fiona, physics).	         	
studies(alice, english).
studies(angus, english).
studies(amelia, drama).
studies(alex, physics).

%teaches(dr_fred, Course), studies(Student, Course). cut nu e folosit 
%teaches(dr_fred, Course), !, studies(Student, Course). cut e folosit, se incheie totul la history, nimeni nu studiaza istoria => returneaza false
%teaches(dr_fred, Course), studies(Student, Course), !. cut e folosit, se incheie la primul false, english alice 
%!, teaches(dr_fred, Course), studies(Student, Course).

%cut in reguli 

max1(X, Y, X) :- X > Y, !.
max1(_X, Y, Y). %X_ e folosit ca sa evitam singleton pattern 

max2(X, Y, X) :- X > Y.
max2(X, Y, Y) :- X =< Y. 

%max1(6, 3, Max).
%max1(3, 6, Max).
%max1(6, 3, 3).


member(X,[X|R]).
member(X,[Y|R]) :- !.


length_list(L, X):-
    length(L, X).


 reverse([],Z,Z).

 reverse([H|T],Z,Acc) :- reverse(T,Z,[H|Acc]).



myPrefix(_, []).
myPrefix([H|T], [H|NT]):-
  myPrefix(T, NT).




sum([ ] , 0).
sum([X|T] ,W) :-
    sum(T, S), W is X+S.


sum2([ ] , 0).
sum2([X|T] ,W) :-
    sum2(T, S), W is X*X+S.


suma_si_sumap(L,Sum,SQS):-
    write(sum(L,Sum)),
    SQS = sum2(L,Y).



ssort([],[]).
ssort([M1|S],[H|T]):-min(H,T,M1),remove(M1,[H|T],N),ssort(S,N).

min(M,[],M).
min(M,[H|T],M1):-min2(M,H,N),min(N,T,M1).

min2(A,B,A):-less(A,B).
min2(A,B,B):-not(less(A,B)).

less(A,B):-(A<B).

append([],B,B).
append([H|A],B,[H|AB]):-append(A,B,AB).

remove(X,L,N):-append(A,[X|B],L),append(A,B,N).