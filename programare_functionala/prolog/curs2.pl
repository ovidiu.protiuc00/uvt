% membru(X,Y) --> true daca X se gaseste in lista Y.

% cazul de baza []--> ? NU; conditia de oprire cand X e primul element din lista
membru(X,[X|_]).
% cazul general --> daca nu e primul element din lista, atunci cautam in
        % tailul listei
membru(X,[_|T]):-membru(X,T).


% lungimea unei liste
% varianta recursiva
len([],0). % cazul de baza pentru []
len([_|T],Rez):-
        len(T,Rez1), Rez is Rez1 + 1.   %cazul general
     % calculam lungimea tailului listei --> Rez1
     % specificam: Rez este 1 + Rez1


% varianta cu acumulator
lenAcc([],A,A).
lenAcc([_|T],A,R):-A1 is A+1,lenAcc(T,A1,R).

len2(L,R):-lenAcc(L,0,R).



% inversa unei liste
%  Ex: [a,b,c] --> [c,b,a]
%   H=a, T=[b,c] --> inversez T: [c,b], apoi inseram la final pe a
%  varianta recursiva
inv([],[]). %cazul de baza []--> inversa listei vide e lista vida
inv([H|T],R):-inv(T,R1), append(R1,[H],R).

%  varianta cu acumulator
%  [1,2,3] []    R
%  [2,3]   [1]   R
%  [3]     [2,1] R
%  []      [3,2,1] R
invAcc([],A,A).
invAcc([H|T],A,R):-invAcc(T,[H|A],R).

inv2(L,R):-invAcc(L,[],R).

% concatenare 2 liste
ap([],L,L).
ap([H|T],L,[H|Z]):- ap(T,L,Z).


%maxim 
max1(X,Y,X):- X>=Y.
max1(X,Y,Y):- X<Y.

%maxim cut
max(X,Y,X):- X>=Y, !.
max(X,Y,Y).