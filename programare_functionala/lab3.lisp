;1
(defun vocale (list)
    (cond
    ((null list) nil)
    ((or (eq 'a (car list))
         (eq 'e (car list))
         (eq 'i (car list))
         (eq 'o (car list))
         (eq 'u (car list)))
         (vocale (cdr list)))
    (t (cons (car list)(vocale (cdr list))))
    )
)
;2
; recursiva pt patrate
(defun suma (l)
	(if (endp l)
		0
		(if (numberp (car l))
		    (+ (suma (cdr l)) (* (car l) (car l)))
		    (suma (cdr l))
		)
	)
)



; varianta final recursiva pt patrate
(defun suma1 (l rez)
	(if (endp l)
		rez
		(if (numberp (car l))
		    (+ (suma (cdr l)) (* rez (car l)))
		    (suma (cdr l))
		)
	)
)
(defun suma2(l)
    (suma1 l (car l))
)

;3
(defun palindrom(l v)
	(if (equal l (reverse v))
	(list 'e 'ok)
	(list 'nu 'e 'ok)
	)

)

(defun x()
(do ((v1 1 (+ 1 v1))
	(v2 () (cons 'a v2))
	)
	((> v1 5) (return v2) (print 'end))
  (print v1)
))