;1
(defun rangeListThreeFive(a b)
(setq l (x a b))
(setq list1 (list 0))
(loop for x in l
    if (or (equal 0 (mod x 5)) (equal 0 (mod x 3)))
        do (setq list1 (append list1 (list x)))
)
 (reverse (setq list1 (cdr list1)))
)

(defun x(a b)
(if (< a b) 
(do ((v1 a (+ 1 v1))
	(v2 () (cons v1 v2))
	)
	((> v1 b) (return v2))
  )
(do ((v1 b (+ 1 v1))
	(v2 () (cons v1 v2))
	)
	((> v1 a) (return v2))
  )
  )
)






;2
; &optional = daca lipseste in apelul de functie atunci se utilizeaza nil in locul lor, sau in cazul nostru y o sa fie 3, daca nu punem nimic
; (f 4)
; > (4 3 NIL NIL NIL)
; &key = perechi cheie valoare, cuvintele chieie la apel au ':', sunt optionale, dar pot aparea in orice ordine la apel
; (f 5 2 :c 11)
; > (5 2 (:C 11) 11 NIL)
; &rest = este folosit pentru functiile cu un numar necunoscut de argumente la definitie, se foloseste abia dupa legarea celorlate argumente, astfel putem avea doar un argument de tip rest
; (f 6 7 :c 8 :d 12 :c 13 :d 32) - in f putem sa punem oricate elemente doar de tip key, 
; > (6 7 (:C 8 :D 12 :C 13 :D 32) 8 12) 
(defun f ( x &optional (y 3) &rest r &key c d)
(list x y r c d))




;3
(defun suma (list)
  (cond ((null list) 0)                    
        ((atom (first list)) (+ (first list)        
            (sum (rest list)))) 
            (t (+ (sum (first list))
        (sum (rest list)))
)
)
)

;; ; incercare pt 3
;; (defun y(l)
;; (setq list1 (list 0))
;; (loop for x in l
;;     if (numberp x)
;;         do (setq list1 (append list1 (list x)))
;; )
;;  (setq list1 (cdr list1))
;; )
