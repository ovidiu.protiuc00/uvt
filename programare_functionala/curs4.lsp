; 8
(do ((v1 1 (+ 1 v1))
	 (v2 () (cons 'a v2))
	 (v3 9)
	 )
	 ((> v1 5) (print v3) v2)
	 (print v1)
)

; 9
(do ((v1 1 (+ 1 v1))
	 (v2 () (cons 'a v2))
	 (v3 9)
	 )
	 ((> v1 5) )
	 (print v1)
)

; 10
(do ((v1 1 (+ 1 v1))
	 (v2 () (cons 'a v2))
	 (v3 9)
	 )
	 ((print v3) )
	 (print v1)
)
;11
(do ((v1 1 (+ 1 v1))
	 (v2 () (cons 'a v2))
	 (v3 9)
	 )
	 ((print v3) 33)
	 (print v1)
)

#|
;12 
(do ((v1 1 (+ 1 v1))
	 (v2 () (cons 'a v2))
	 (v3 9)
	 )
	 ((print nil))
	 (print v1)
)

;13
(do ((v1 1 (+ 1 v1))
	 (v2 () (cons 'a v2))
	 (v3 9)
	 )
	 (print v1)
)
|#

; suma numerelor unei liste

#|
> (suma-it '(1 2 3 4 a b 5))
15
> (suma-it '(55 g))
55
> (suma-it '())
0
> (suma-it '(a b c))
0
|#



