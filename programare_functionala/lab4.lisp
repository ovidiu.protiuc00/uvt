(defun ex1()
    (when (= 8 9) 4 9 10 12)
)

(defun ex2()
    (when (< 8 9) 9)
)

(defun ex3()
    (when (> 10) 9)
)

(defun ex4()
    (unless (> 10 9) 'nothing)
)

(defun ex5()
    (unless (< 10 9) 'nothing)
)

(block block1
  (print 'hello)     ; evaluated
  (return-from block1)
  (print 'goodbye))  ; not evaluated
;;=> NIL

(block block2
 (print 'hello)     ; evaluated
  (return-from block2 42)
  (print 'goodbye))  ; not evaluated
;;=> 42

(block block3
(setq x 1)
(print (1+ x ))
(return-from block3 (1+ x))
(print 4)
)

(defun ex6()
 (dolist (x '(1 2 3)) (print x))
)

(defun ex7()
 (dolist (x '(2 3 -7 5)) 
    (print x)
    (if (< x 0)
        (return 'gata)
        )
    )
)

(defun ex8()
    (tagbody reia
        (if ( plusp ( read ))
        (go reia)
        (print 1000)
        )
    )
)

(defun ex9() 

    (setq w 11 xx 22)
    (values w xx)
    (values 1 2 3 4)
    ;(values '(1 2 3 4))
    ;(progn 10 ( print 20 ) 30 )
    (prog1 1 2 3 4 5)
    (progn 1 2 3 4 5)
)

;;let => actioneaza in paralel
(defun ex10()
    (let ((x 1) (y 2) (z 3)) (setq w(+ x y z)) (list x y z w))
)

;;let* => actioneaza secvential 
(defun ex11()
    (let* ((x 1) (y (+ x 1)) (z (+ y 1))) (list x y z))
)


;;prog este combinatie intre block, tagbody si let*
;;prog deschide implicit un block cu numele nil, astfel ca se poate iesi din el cu return
(defun ex12()
    (prog (i (suma 0))
        reia
        (setq i (read))
        (if (> i 0)
            (progn (setq suma (+ suma i)) (go reia))
            (return suma)
        )
    )
)


(defun ex13(start sfarsit)

    (do ((i start (+ 1 i)))
        ((> i sfarsit))
        (print i)
    )

)

(defun factorial (n)

    (do* ((i 1 (+ 1 i))
        (rezultat 1 (* rezultat i)))
        ((= i n) rezultat)
    )
    
)

(defun ex14()
    (loop for i from 1 to 6 do (print i))
)
