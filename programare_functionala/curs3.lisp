;;;;; factorial ;;;;;;;;;

; varianta recursiva
(defun factorial (n)
	(if (<= n 0)
		1 
		(* (factorial (- n 1)) n)
	)
)

#|
> (factorial 200)

> (time (factorial 3000))

|#


; varianta final recursiva (folosind tehnica variabilei colectoare)
(defun factf (n)
	(fact-aux n 1)
)

(defun fact-aux (n rez)
	(if (= n 0)
		rez
		(fact-aux (- n 1) (* rez n) )
	)
)

#|
> (fact-aux 5 10)
1200
> (factf 5)
120
|#

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;   fibonacci   ;;;;;;;;;;;;;;;;;;;;;;
; varianta recursiva
(defun fib (n)
	(if (<= n 1)
		n
		(+ (fib (- n 1)) (fib (- n 2)))
	)
)

; (fib 10)

;;;;;;;;;;;;;
; varianta final recursiva

(defun rfib (n)
	(if (< n 2)
		n
		(fib-aux n 1 0)
	)
)

(defun fib-aux (n fn fn-1)
	(if (= n 1)
		fn
		(fib-aux (- n 1) (+ fn fn-1) fn )
	)
)

#|
> (untrace fib)
(FIB)
> (trace fib-aux)
;; Tracing function FIB-AUX.
(FIB-AUX)
> (rfib 5)
|#


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;    lungimea unei liste   ;;;;;;;;;;;
; varianta recursiva
(defun len (l)
	(if (endp l)
		0
		(+ 1 (len (cdr l)))
	)
)
#|
> (len '(5 a b c 4))
|#

; varianta final recursiva

(defun lenf (l)
	(len-aux l 0) ; 0 reprezinta valoarea initiala a variabilei colectoare
)

(defun len-aux (l rez)
	(if (endp l)
		rez
		(len-aux (cdr l) (+ 1 rez) )
	)
)

#|
> (lenf '(a b c))
3
> (lenf '(a b (1 2) 5àS ))
4
> (lenf '(a b (1 2) 5))
4
> (trace len-aux)
;; Tracing function LEN-AUX.
(LEN-AUX)
> (lenf '(a b c))
|#


; suma numerelor unei liste
; TEMA varianta final recursiva


; inversa unei liste (recursiv si final recursiv)
;varianta recursiva
(defun inv (l)
	(if (endp l)
		nil
		(concatenare (inv (cdr l)) (list (car l)) )
	)
)

; varianta final recursiva
#|
Exemplu:
 lista: '(a b c) ==> rez: '(c b a)
'(a b c)  '()
'(b c)    '(a)
'(c)      '(b a) ; inserarea lui b la inceputul '(a)
'()       '(c b a) ; avem rezultatul in variabila colectoare
pentru construirea solutiei nu mai e nevoie de utilizarea functiei de concatenare
ca in varianta recursiva
|#

(defun invf (l)
	(inv-aux l '())
)

(defun inv-aux (l rez)
	(if (endp l)
		rez
		(inv-aux (cdr l) (cons (car l) rez) )
	)
)

#|
 (invf '(a b c))
(C B A)
> (trace inv-aux)
;; Tracing function INV-AUX.
(INV-AUX)
> (invf '(a b c))
|#



; concatenarea a doua liste
; varianta recursiva

(defun concatenare (l1 l2)
	(if (endp l1)
		l2
		(cons (car l1) (concatenare (cdr l1) l2))
	)
)
; TEMA varianta final recursiva
