;suma numerelor unei liste
(defun suma (l)
	(if (endp l)
		0
		(if (numberp (car l))
		    (+ (suma (cdr l)) (car l))
		    (suma (cdr l))
		)
	)
)


; media aritmetica

(defun lungime(l)
	(if (endp l)
		0
		(if (numberp (car l))
			(+ (suma (cdr l)) 1)
			(suma (cdr l))
		)
		
	)

)



(defun medie(l)
   (/ suma '(l) lungime '(l))
)

(defun prima_aparitie(l)
   (cdr l)
)
