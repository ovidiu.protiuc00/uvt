;1
(defun cdr(lst)
    (if len(lst) == 1)
    (return 'nil)
    (return lst[1:])
)
;2
(defun maxx(n)
    (loop for x in n
      maximize x) 
)

;3
(defun factorial (n)
	(if (<= n 0)
		1 
	    (* (factorial (- n 1)) n)
	)
)


;4
(defun even (n)
    (remove-if-not (lambda (x) (evenp x)) n)
)


;5
(defmacro aduna ( nr &rest i )
(+ nr (apply '+ i ) ) 
)
