;variabile legate, libere, defun
;creare, compilare

(defun print-list-2 (el1 el2)
	"creaza o lista cu 2 elem"
	(list el1 el2)
)

(defun inv2 (lista)
	(print-list-2 (cadr lista) (car lista))
)

(defun impar-div-3 (nr)
	(and (oddp nr) (equal 0 (mod nr 3)))
)

; impar si divizibil cu 3

;exemplu confuz

(defun conf (l)
	(list l)
)

;factorialul unui nr natural
#|
; pt cel mai mic obiect (n=0) STOP 0!=1
; apelul recursiv: pe un nr < decat cel initial
n! = 1 * 2 *... * (n-1) *n = (n-1)! * n. 

n=4     ... *4 = 6 * 4 = 24
3       ... *3 = 2 * 3 = 6
2	... *2 = 1 * 2 = 2
1	... *1 = 1
0	1   STOP
|#

(defun factorial (n)
	(if (<= n 0)
		1 
		(* (factorial (- n 1)) n)
	)
)


;lungimea unei liste

; (a (1 2 3) b c) ==> 4
; (d f) ==> 2

; nil ==> STOP
; apelul recursiv pe tailul listei -> cdr

(defun len (l)
	(if (endp l) 
		0
		(+ (len (cdr l)) 1)
	)
)

; suma numerelor unei liste
;(1 2 3 4) ==> 10  
; (1 2 a 4) ==> 7
(defun suma-aux (l rez)
	(if (endp l)
		rez
		(if (numberp (car l))
			(if (> (car l) 0)
		    	(suma-aux (cdr l) (+ (car l) rez)) 
		    	(suma-aux (cdr l) rez)
				)
				(suma-aux (cdr l) rez
		)
	)
))

(defun suma (l)
	(suma-aux l 0)
)
; suma numerelor unei liste la orice nivel
; (1 (2 3) ((4))) ==> 10 TEMA


;trace/ untrace
;longer list
;cate numere apar intr-o lista

(defun nrnr (l)
	(if (endp l)
		0
		(if (numberp (car l))
		    (+ (nrnr (cdr l)) 1)
		    (nrnr (cdr l))
		)
	)
)

;concatenarea a doua liste
; '() l2
; (a b c)   (1 2 3 4) ==> (a b c 1 2 3 4)
;                  (cons (car l1) (conc (cdr l1) l2)

(defun conc (l1 l2)
	(if (endp l1)
		l2
		(cons (car l1) (conc (cdr l1) l2))
	)
)

(defmacro suma-it (l)
	(do ((lista l (cdr lista))
		 (suma 0 
		 (if (numberp (car lista)) (if (> (car l) 0)
		 (+ (car lista) suma) suma)))
		)
		((endp lista) suma)
	)
)
