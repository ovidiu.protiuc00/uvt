import sys, socket
import util

HOST = '127.0.0.1'
PORT = util.PORT

if __name__ == '__main__':
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((HOST, PORT))
    except ConnectionError:
        print('Socket error on connection')
        sys.exit(1)

    print('\nConnected to {}:{}'.format(HOST, PORT))
    argument = 0
    if len(sys.argv) > 1:
        argument = sys.argv[1]
    try:
        util.send_msg(sock, argument) # Blocks until sent
        msg = util.recv_msg(sock)
        print('Received echo: ' + msg)
    except ConnectionError:
        print('Socket error during communication')
        sock.close()
        print('Closed connection to server\n')
    print("Closing connection")
    sock.close()

    print("Closing connection")
    sock.close()
