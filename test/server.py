import util,time, sys
import threading


HOST = util.HOST
PORT = util.PORT
COUNT = 0
def increment(val):
    global COUNT
    COUNT = COUNT+val
    return COUNT
def handle_client(sock, addr):
    try:
        msg = util.recv_msg(sock) # Blocks until received

        print('Client accepted {}: {}'.format(addr, msg))
        value = str(COUNT) + "-" + str(increment(int(msg)-1))
        increment(1)
        util.send_msg(sock, value)
        print('connection closed {}'.format(addr))
    except (ConnectionError, BrokenPipeError):
        print('Closed connection to {}'.format(addr))
        sock.close()
    return msg



if __name__ == '__main__':
    if len(sys.argv) > 1:
        if isinstance(sys.argv[1],int):
            COUNT = int(sys.argv[1])
    listen_sock = util.create_listen_socket(HOST, PORT)
    addr = listen_sock.getsockname()
    print('Listening on {}'.format(addr))
    while True:
        client_sock, addr = listen_sock.accept()
        # Thread will run function handle_client() autonomously
        # and concurrently to this while loop
        thread = threading.Thread(target = handle_client, args = [client_sock, addr], daemon=True)
        thread.start()