# Tema 
# Problema5 (Lab 11)
#varianta 1 - in scris
#date populatie
mu=75
#date esantion de aceasta data sunt cunoscute
x=c(65,78,88,55,48,95,66,57,79)
media=mean(x)
media
s=sd(x)
s
n=length(x)
#test de inferenta a mediei NU se cunoaste abaterea standard - testul t
t=(media-mu)/(s/sqrt(n))
t
#valoarea critica
#nivel de �ncredere 90%, atunci nivel de neincredere 10%
alfa=0.05
#z urmeaza repartitia normala standard
valoare_critica=-qt(1-alfa/2,n-1)
valoare_critica

#varianta 2 - folosind instructiuni de teste in r
mu1=75
x=c(65,78,88,55,48,95,66,57,79)

t.test(x,mu=mu1, alternative="two.sided", conf.level=0.95)
# ------------------------------------------------------------------



#Lab 11
# Problema 4
df=15
#test bilateral
t=-1.84
#t este negativ
#probabilitatea sa fie mai mica decat t
pt(t,df)
# probabilitatea sa fie mai mare decat -t
pt(-t,df,lower.tail = FALSE)
#pvaloarea
pt(t,df) + pt(-t,df,lower.tail = FALSE)
2*pt(t,df)

#Lab13
# Problema 4
?t.test
x=c(110, 122, 140, 120, 124, 95, 93, 105, 104, 110)
y=c(120, 124, 104, 144, 134, 110, 132, 100, 98, 110)
t.test(x,y,alternative = c("less"),
       mu = 0, paired = FALSE, var.equal = FALSE,
       conf.level = 0.90)

#Lab14
# Problema 1
x = c(2, 3, 3, 4, 4, 5, 5, 6, 6, 6, 7, 7, 7, 8, 8)
y = c(5, 5, 7, 5, 7, 7, 8, 6, 9, 8, 7, 9, 10, 8, 9)
cor(x,y)

# Problema 2
?mtcars
data = data.frame(mtcars)

# Vizualizarea setul de date, retinem variabilele din setul de date.
names(data)
head(data)

mpg = data$mpg
cyl = data$cyl
disp = data$disp
hp = data$hp
drat = data$drat
wt = data$wt
qsec = data$qsec
vs = data$vs
am = data$am
gear = data$gear
carb = data$carb

# Afisam grafic datele (diagrama de imprasitere/norul de puncte) folosind instructiunea plot.
# Calculam coef. de corelatie pentru diferite seturi de date, alegand variabile
#care prezinta corelatii liniare pozitive sau negative (variabile bine corelate sau mai putin corelate).
plot(mpg, cyl)
cor(mpg, cyl)

plot(mpg, hp)
cor(mpg, hp)
plot(mpg, wt)
cor(mpg, wt)
plot(disp, wt)
cor(disp, wt)

par(mar=c(1,1,1,1))
pairs(data)
plot(mpg, disp)
cor(mpg, disp)

# Putem calcula coeficientul de corelatie pentru toate perechile posibile
#cu instructiunea cor(data) care are ca output matricea de corelatie
cor(data)

# sa se verifice corelatia liniara folosind testul Pearson (folosind instructiunea
#cor.test)
# H_0: cele doua variabile sunt necorelate liniar (rho = 0)
# H_a: cele doua variabile sunt corelate (rho != 0)
# corelatia intre mpg = consumul de combustibili si disp = volumul total al motorului
#(cuantificand circumferinta cilindrilor, adancimea, numarul lor)
cor.test(mpg, disp)
Pearson_test = cor.test(mpg, disp)
Pearson_test$p.value < 0.05
# p-val<alpha, deci respingem ipoteza nula H_0 si tragem concluzia ca variabilele mpg si disp sunt puternic corelate negativ
#cu un coeficient de corelatie = -.0847 cu un nivel de incredere de 95%

# Problema 4
# Vrem sa determinam coeficientii b0(ordonata la origine) si b1(panta de regresie)
#folosim functia linear model lm()
# Practic, vrem sa descriem variabila miles/gallon folosind variabila disp
lm(mpg ~ disp, data)
# Interpretare output
# call = functia folosita in modelul de regresie (mpg = b0 + b1*disp)
# Coefficients: b0 = 29.59985, b1 = -0.04122
# Ecuatia de regresie liniara estimata este: mpg = 29.59985-0.04122*disp
fit = lm(mpg ~ disp)
coef = coefficients(fit)
coef
eq = paste("y = ", coef[2], "*x+ ", coef[1])  #dreapta de regresie folosind outputul din lm()
eq
plot(disp, mpg)
abline(fit) # se traseaza o dreapta de parametrii coef din eq (abline(a, b) traseaza graficul lui y = a+bx)

#Testam modelul cu summary()
summary(fit)
# Observatii:
# 1. Pr(>|t|) = valoarea unui test statistic cu H0: coeficientii sunt 0 (deci nu exista realtie semnificativa statistic intre mpg si disp)
#Pr(>|t|) < 0.05, deci exista o relatie semnificativa statistic intre variabila descrisa(mpg) si predictor (disp)

# 2.  Adjusted R-squared:  0.709 inseamna in ce masura modelul explica datele (70%)
# 3.  p-value < 0.05 => volumul total al motorului este un estimator bun pentru consumul de combustibili


