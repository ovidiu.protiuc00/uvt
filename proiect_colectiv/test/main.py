from flask import Flask, redirect, url_for, request, render_template, session, flash
from datetime import timedelta
app = Flask(__name__)
app.secret_key = 'hwellore'
app.permanent_session_lifetime = timedelta(minutes=5)
@app.route('/')
def home():
   return render_template('index.html')

@app.route('/login',methods = ['POST', 'GET'])
def login():
    print(1)
    if request.method == 'POST':
        print(2)
        session.permanet = True
        user = request.form['nm']
        session['user'] = user
        return redirect(url_for('user'))
    else:
        if 'user' in session:
            print(3)
            return redirect(url_for('user'))
    print(5)
    return render_template('login.html')

@app.route('/user',methods = ['POST', 'GET'])
def user():
    if 'user' in session:
        user = session['user']
        flash('Hei', 'error')
        return render_template('index.html')
    else:
        return redirect(url_for('index'))

@app.route('/logout',methods = ['POST', 'GET'])
def logout():
    session.pop('user', None)
    flash('Ai plecat', 'info')
    return redirect(url_for('login'))


if __name__ == '__main__':
   app.run(debug = True) 